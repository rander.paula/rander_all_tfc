package com.example.rander_all_tfc

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.EditText
import android.widget.Toast

val DATABASE_NAME = "MyDB"
val TABLE_NAME = "table"
val TASK_NAME = "name"

class Database_Helps(var context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,1){
    override fun onCreate(p0: SQLiteDatabase?) {
        val createTable = "CREATE TASK " + TABLE_NAME + "("+TASK_NAME + ")"
        p0?.execSQL(createTable)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        TODO("Not yet implemented")
    }

    fun insertData(UID: ToDoModelo) {
        val p0 = this.writableDatabase
        var cv = ContentValues()
        cv.put(TASK_NAME,UID.itemDataText)
        var result = p0.insert(TABLE_NAME,null,cv)
        if(result == -1.toLong())
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show()
    }

}


