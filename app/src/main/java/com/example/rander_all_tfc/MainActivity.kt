package com.example.rander_all_tfc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    var toDOList:MutableList<ToDoModelo>?=null
    lateinit var adapter: ToDoAdapter
    private var listViewItem : ListView?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val context = this
        val fab = findViewById<View>(R.id.fab) as FloatingActionButton //Variavel para o botao de inserção
        listViewItem= findViewById<ListView>(R.id.item_textView)

        fab.setOnClickListener{ view ->
            val alertDialog=AlertDialog.Builder(this) //Caixinha de entrada
            val textEditText = EditText(this) //Entrada de texto
            alertDialog.setMessage("Add a Todo Item ")
            alertDialog.setTitle("Enter To Do item")
            alertDialog.setView(textEditText)
            alertDialog.setPositiveButton("Add"){dialog,i->
                val todoItemData=ToDoModelo.createList()
                todoItemData.itemDataText=textEditText.text.toString()
                todoItemData.done = false
                var p0 = Database_Helps(context)
                p0.insertData(todoItemData)
                dialog.dismiss()
                Toast.makeText(this,"item saved", Toast.LENGTH_LONG).show()


            }
            alertDialog.show()
        }


    }
}